package com.example.silviu.photolist;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;


public class current_list extends ActionBarActivity {

    final static String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/items";
    public static final String KEY = "key";
    public static final int LIST_REQUEST = 1;
    public static final  int LIST_RESULT = 2;

    ArrayList<String> photoNames;
    ListView currentItems;
    ArrayList<Bitmap> images;
    SpecialAdapter adapter;
    File file;
    EditText extra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_list);
        currentItems = (ListView)findViewById(R.id.currentItems);
        photoNames = new ArrayList<String>();
        images = new ArrayList<Bitmap>();

        adapter = new SpecialAdapter(current_list.this, images);
        currentItems.setAdapter(adapter);
        file = new File(file_path);
        if(!file.exists())
        {
            file.mkdir();
        }

        LoadMyList();

        currentItems.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                photoNames.remove(position);
                try {
                    LoadImages(photoNames);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            }
        });

        Button addItem = (Button)findViewById(R.id.addItem);
        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(current_list.this, possible_items.class);
                startActivityForResult(intent, LIST_REQUEST);
            }
        });


        Button clearList = (Button)findViewById(R.id.clearAllCurrent);
        clearList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoNames.clear();
                images.clear();
                adapter.notifyDataSetChanged();
            }
        });

        extra = (EditText)findViewById(R.id.specialInstructions);
        extra.setVisibility(View.GONE);
        Button togglr = (Button)findViewById(R.id.toggler);
        togglr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(extra.getVisibility() != View.GONE)
                {
                    extra.setVisibility(View.GONE);
                }else
                {
                    extra.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void LoadMyList()
    {
        SharedPreferences list = getSharedPreferences("SetKey", 0);
        String x = list.getString("SetKey", "");
        //Log.v("Silviu", "Loading" + x);
        if(!x.equals(""))
        {
            String[] g =  x.split("-");

            for(int i = 0; i<g.length; i++)
            {
                //Log.v("Silviu", g[i]);
                photoNames.add(g[i]);
            }
        }


        try {
            LoadImages(photoNames);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop()
    {
        SaveMyList();
        super.onStop();
    }

    private void SaveMyList()
    {
        SharedPreferences list = getSharedPreferences("SetKey", 0);
        SharedPreferences.Editor editor = list.edit();
        String x = "";
        if(photoNames.size()>0)
        {
            x = photoNames.get(0);
        }
        for(int i = 1; i<photoNames.size(); i++)
        {
            x = x + "-" + photoNames.get(i);
        }

        //Log.v("Silviu", "SAVING");

        editor.putString("SetKey", x);
        editor.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == LIST_REQUEST && resultCode == LIST_RESULT)
        {
            try {
                photoNames = (ArrayList<String>)data.getExtras().get(KEY);
                LoadImages(photoNames);
                //Log.v("SILVIU", photoNames.size()+"");
            }catch(Exception e)
            {
                return;
            }
        }
    }

    void LoadImages(ArrayList<String> ImageNames) throws IOException
    {
        images.clear();

        for(int i = 0; i<ImageNames.size(); i++) {
            try{
            File image = new File(file_path + "/" + photoNames.get(i));
            Bitmap myBitmap = BitmapFactory.decodeFile(image.getAbsolutePath());

            //Log.v("SILVIU IN CL", adapter.getCount() + "");
            images.add(myBitmap);

            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_current_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            Log.d("Activity", "Touch event "+event.getRawX()+","+event.getRawY()+" "+x+","+y+" rect "+w.getLeft()+","+w.getTop()+","+w.getRight()+","+w.getBottom()+" coords "+scrcoords[0]+","+scrcoords[1]);
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom()) ) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }
}
