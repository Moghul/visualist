package com.example.silviu.photolist;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;


public class possible_items extends ActionBarActivity {

    final static String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/items";
    final static int KEY = 0;
    ListView possibleItems;
    SpecialAdapter adapter;
    ArrayList<String> photoNames;
    ArrayList<Bitmap> images;
    File file;
    ArrayList<String> toBeSent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_possible_items);
        photoNames = new ArrayList<String>();
        toBeSent = new ArrayList<String>();
        images = new ArrayList<Bitmap>();

        file = new File(file_path);
        if(!file.exists())
            file.mkdirs();

        possibleItems = (ListView)findViewById(R.id.possibleItems);
        adapter = new SpecialAdapter(possible_items.this, images);
        possibleItems.setAdapter(adapter);

        try {
            LoadImages();
        } catch (IOException e) {
            e.printStackTrace();
        }

        possibleItems.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                toBeSent.add(photoNames.get(position));
            }
        });


        possibleItems.setOnItemLongClickListener(new ListView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if(file.list().length>0)
                {
                    String x = photoNames.get(position);
                    String[] pics = file.list();
                    for(int i = 0; i<pics.length; i++)
                    {
                        File toBeDeleted = new File(file_path+"/"+pics[i]);
                        //Log.v("Silviu LazyAdapter", photoNames.get(position) + "   =   " + toBeDeleted.getName());
                        if(x.equals(toBeDeleted.getName()))
                        {
                            DeleteRecursive(toBeDeleted);
                            try {
                                LoadImages();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                return true;
            }
        });

        Button returnToList = (Button)findViewById(R.id.returnToList);
        returnToList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(possible_items.this, current_list.class);
                intent2.putExtra(current_list.KEY, toBeSent);
                setResult(current_list.LIST_RESULT, intent2);
                finish();
            }
        });

        Button createNewItem = (Button)findViewById(R.id.createItem);
        createNewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if(intent.resolveActivity(getPackageManager()) != null){
                    startActivityForResult(intent, KEY);
                }
            }
        });


        Button deleteAll = (Button)findViewById(R.id.deleteAll);
        deleteAll.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DeleteRecursive(file);
                    try {
                        LoadImages();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        //super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == KEY)
        {
            Bitmap bitmap;
            try{
                 bitmap = (Bitmap)data.getExtras().get("data");
            }catch (Exception e)
            {
                return;
            }

            try {
                SaveImage(bitmap);
                LoadImages();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void DeleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                DeleteRecursive(child);

        fileOrDirectory.delete();
    }

    void SaveImage(Bitmap bitmap) throws IOException {
        Calendar c = Calendar.getInstance();
        File dir = new File(file_path);
        if(!dir.exists())
        {
            dir.mkdirs();
        }
        File file = new File(dir, "Item" + c.get(Calendar.YEAR) + c.get(Calendar.MONTH) +
                c.get(Calendar.DAY_OF_MONTH) + c.get(Calendar.HOUR) + c.get(Calendar.MINUTE) +
                c.get(Calendar.SECOND) + ".png");

//        Log.v("Silviu Saving", "Item" + c.get(Calendar.YEAR) + c.get(Calendar.MONTH) +
//                c.get(Calendar.DAY_OF_MONTH) + c.get(Calendar.HOUR) + c.get(Calendar.MINUTE) +
//                c.get(Calendar.SECOND) + ".png");

        FileOutputStream fOut = new FileOutputStream(file);

        bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);

        if(fOut!=null){
            fOut.flush();
        }
        fOut.close();
    }

    void LoadImages() throws IOException
    {
        photoNames.clear();
        images.clear();

        if(!file.exists())
        {
            file.mkdir();
        }
        String[] x = file.list();
        if(x.length>0)
        {
            for(int i = 0; i<x.length; i++)
            {
                photoNames.add(x[i]);
            }

            for(int i = 0; i< photoNames.size(); i++)
            {
                File image = new File(file_path + "/" + photoNames.get(i));
                Bitmap myBitmap = BitmapFactory.decodeFile(image.getAbsolutePath());

                images.add(myBitmap);
            }
        }
        adapter.notifyDataSetChanged();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_possible_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
