package com.example.silviu.photolist;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;

public class SpecialAdapter extends BaseAdapter {
    private ArrayList<Bitmap> imgs = new ArrayList<Bitmap>();
    private LayoutInflater l_Inflater;

    public SpecialAdapter(Context context, ArrayList<Bitmap> images) {
        imgs = images;
        l_Inflater = LayoutInflater.from(context);
    }

    public int getCount() {
        if(imgs!=null){
        return imgs.size();
        }
        else
        {
            return 0;
        }
    }

    public Bitmap getItem(int position) {
        return imgs.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = l_Inflater.inflate(R.layout.possible_item, null);
            holder = new ViewHolder();
            holder.itemImage = (ImageView) convertView.findViewById(R.id.imageView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.itemImage.setImageBitmap(imgs.get(position));

        return convertView;
    }

    static class ViewHolder {
        ImageView itemImage;
    }
}

